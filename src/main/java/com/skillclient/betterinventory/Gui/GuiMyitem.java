 package com.skillclient.betterinventory.Gui;
 
 import java.awt.Color;
 import java.io.IOException;
 import java.util.List;

 import com.skillclient.betterinventory.Tabs.TabMyItem;
 import com.skillclient.betterinventory.util.Chat;
import com.skillclient.betterinventory.util.ItemStacks;

 import net.minecraft.client.Minecraft;
 import net.minecraft.client.entity.EntityPlayerSP;
 import net.minecraft.client.gui.GuiButton;
 import net.minecraft.client.gui.GuiScreen;
 import net.minecraft.client.gui.GuiTextField;
 import net.minecraft.item.ItemStack;
 
 public class GuiMyitem extends GuiScreen
 {
   GuiTextField textfield;
   ItemStack stack;
   String defaultItem;
   
   public GuiMyitem()
   {
     this.defaultItem = "minecraft:diamond 1 0 {display:{Name:\"&2Your item\"}}";
     this.stack = new ItemStack(net.minecraft.init.Blocks.BARRIER);
   }

   public void initGui()
   {
     this.buttonList.add(new GuiButton(-1, 10, this.height - 30, 100, 20, "Config-date"));
     this.buttonList.add(new GuiButton(0, this.width - 120, this.height - 30, 50, 20, "Back"));
     this.buttonList.add(new net.minecraftforge.fml.client.config.GuiButtonExt(1, this.width / 2 + 155, this.height / 2 + 50, 60, 20, "Give"));
     this.textfield = new GuiTextField(2, this.fontRenderer, this.width / 2 - 150, this.height / 2 + 50, 300, 20);
     this.textfield.setFocused(true);
     this.textfield.setMaxStringLength(1000000);
     this.textfield.setText(this.defaultItem);
     this.textfield.setCursorPosition(45);
     this.stack = ItemStacks.stringtostack(this.defaultItem);
     this.textfield.updateCursorCounter();
     this.buttonList.add(new GuiButton(3, this.width - 180, this.height - 30, 50, 20, "Save Item"));
     this.buttonList.add(new GuiButton(-2, this.width - 240, this.height - 30, 50, 20, "Save Inv"));
     this.buttonList.add(new GuiButton(4, 120, this.height - 30, 100, 20, "Online-Generators"));
     
 
 
     this.buttonList.add(new GuiButton(100, 5, this.height / 2 + 25, 20, 20, Chat.code(0) + "&0"));
     this.buttonList.add(new GuiButton(101, 30, this.height / 2 + 25, 20, 20, Chat.code(1) + "&1"));
     this.buttonList.add(new GuiButton(102, 55, this.height / 2 + 25, 20, 20, Chat.code(2) + "&2"));
     this.buttonList.add(new GuiButton(103, 80, this.height / 2 + 25, 20, 20, Chat.code(3) + "&3"));
     this.buttonList.add(new GuiButton(104, 105, this.height / 2 + 25, 20, 20, Chat.code(4) + "&4"));
     this.buttonList.add(new GuiButton(105, 130, this.height / 2 + 25, 20, 20, Chat.code(5) + "&5"));
     this.buttonList.add(new GuiButton(106, 155, this.height / 2 + 25, 20, 20, Chat.code(6) + "&6"));
     this.buttonList.add(new GuiButton(107, 180, this.height / 2 + 25, 20, 20, Chat.code(7) + "&7"));
     this.buttonList.add(new GuiButton(108, 205, this.height / 2 + 25, 20, 20, Chat.code(8) + "&8"));
     this.buttonList.add(new GuiButton(109, 230, this.height / 2 + 25, 20, 20, Chat.code(9) + "&9"));
     this.buttonList.add(new GuiButton(110, 255, this.height / 2 + 25, 20, 20, Chat.code("a") + "&a"));
     this.buttonList.add(new GuiButton(111, 280, this.height / 2 + 25, 20, 20, Chat.code("b") + "&b"));
     this.buttonList.add(new GuiButton(112, 305, this.height / 2 + 25, 20, 20, Chat.code("c") + "&c"));
     this.buttonList.add(new GuiButton(113, 330, this.height / 2 + 25, 20, 20, Chat.code("d") + "&d"));
     this.buttonList.add(new GuiButton(114, 355, this.height / 2 + 25, 20, 20, Chat.code("e") + "&e"));
     this.buttonList.add(new GuiButton(115, 380, this.height / 2 + 25, 20, 20, Chat.code("f") + "&f"));
     this.buttonList.add(new GuiButton(116, 405, this.height / 2 + 25, 20, 20, Chat.Obfuscated + "&k"));
     this.buttonList.add(new GuiButton(117, 430, this.height / 2 + 25, 20, 20, Chat.Bold + "&l"));
     this.buttonList.add(new GuiButton(118, this.width - 225, this.height / 2, 20, 20, Chat.Strikethrough + "&m"));
     this.buttonList.add(new GuiButton(119, this.width - 200, this.height / 2, 20, 20, Chat.Underline + "&n"));
     this.buttonList.add(new GuiButton(120, this.width - 175, this.height / 2, 20, 20, Chat.Italic + "&o"));
     this.buttonList.add(new GuiButton(121, this.width - 150, this.height / 2, 20, 20, Chat.Reset + "&r"));
   }
   
   public void updateScreen()
   {
     this.textfield.updateCursorCounter();
     super.updateScreen();
   }
   
   protected void keyTyped(char typedChar, int keyCode)
     throws IOException
   {
     switch (typedChar)
     {
     case '\r': 
       start();
       break;
     
     case '&': 
       this.textfield.textboxKeyTyped('&', keyCode);
       break;
     default: 
       this.textfield.textboxKeyTyped(typedChar, keyCode);
     }
     
     
 
     this.stack = ItemStacks.stringtostack(this.textfield.getText());
     super.keyTyped(typedChar, keyCode);
   }
   
   public void drawScreen(int mouseX, int mouseY, float partialTicks)
   {
     net.minecraft.client.renderer.GlStateManager.color(1.0F, 1.0F, 1.0F);
     drawDefaultBackground();
     drawHoveringText("MyItem-Gui", this.width / 2 - 47, 25);
     this.textfield.drawTextBox();
     this.itemRender.renderItemAndEffectIntoGUI(this.stack, this.width / 4, this.height / 2);
     renderToolTip(this.stack, this.width / 4 + 10, this.height / 3);
     this.fontRenderer.drawString(this.stack.getMaxStackSize() + "*", this.width / 4 - 18, this.height / 2 + 5, Color.white.getRGB());
     super.drawScreen(mouseX, mouseY, partialTicks);
   }
   
   static EntityPlayerSP player = Minecraft.getMinecraft().player;
   
   protected void actionPerformed(GuiButton button) throws IOException
   {
     switch (button.id)
     {
       case -2:
         ItemStacks.saveinv();
         break;
       case -1:
         Runtime.getRuntime().exec("notepad.exe " + com.skillclient.betterinventory.Betterinventory.configfile);
         break;
     case 0: 
       onGuiClosed();
       break;
       case 1:
         start();
         break;
       case 3:
         TabMyItem.MyItemStacks.add(ItemStacks.stringtostack(this.textfield.getText()));
         break;
     case 4:
       this.mc.displayGuiScreen(new GuiLinks());
       break;
     
     case 100: 
     case 101: 
     case 102: 
     case 103: 
     case 104: 
     case 105: 
     case 106: 
     case 107: 
     case 108: 
     case 109: 
       this.textfield.textboxKeyTyped('&', 0);
       this.textfield.textboxKeyTyped((char)(button.id - 100), 0);
       break;
     
     case 110: 
     case 111: 
     case 112: 
     case 113: 
     case 114: 
     case 115: 
     case 116: 
     case 117: 
     case 118: 
     case 119: 
     case 120: 
     case 121: 
       char[] ca = { 'a', 'b', 'c', 'd', 'e', 'f', 'k', 'l', 'm', 'n', 'o', 'r' };
       this.textfield.textboxKeyTyped('&', 0);
       this.textfield.textboxKeyTyped(ca[(button.id - 110)], 0);
     }
     
     this.stack = ItemStacks.stringtostack(this.textfield.getText());
   }
   
   void start()
   {
     TabMyItem.MyItemStacks.add(this.stack);
     Minecraft.getMinecraft().player.inventory.addItemStackToInventory(this.stack);
   }
   
   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
     super.mouseClicked(mouseX, mouseY, mouseButton);
     this.textfield.mouseClicked(mouseX, mouseY, mouseButton);
   }
 }

