 package com.skillclient.betterinventory.Gui;
 
 import java.io.IOException;
 import java.util.List;
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.gui.GuiButton;
 import net.minecraft.client.gui.GuiScreen;
 
 public class GuiLinks extends GuiScreen
 {
   public void initGui(){
     this.buttonList.add(new GuiButton(15, this.width / 2 - 125, this.height / 2 - 125, 100, 20, "Potions"));
     this.buttonList.add(new GuiButton(5, this.width / 2 - 125, this.height / 2 - 100, 100, 20, "Tools"));
     this.buttonList.add(new GuiButton(6, this.width / 2 - 125, this.height / 2 - 75, 100, 20, "Armor"));
     this.buttonList.add(new GuiButton(7, this.width / 2 - 125, this.height / 2 - 50, 100, 20, "Item"));
     this.buttonList.add(new GuiButton(8, this.width / 2 - 125, this.height / 2 - 25, 100, 20, "Head/Skull"));
     this.buttonList.add(new GuiButton(9, this.width / 2 + 5, this.height / 2 - 125, 100, 20, "Sign"));
     this.buttonList.add(new GuiButton(10, this.width / 2 + 5, this.height / 2 - 100, 100, 20, "Chest"));
     this.buttonList.add(new GuiButton(11, this.width / 2 + 5, this.height / 2 - 75, 100, 20, "mcstacker"));
     this.buttonList.add(new GuiButton(12, this.width / 2 + 5, this.height / 2 - 50, 100, 20, "mcstacker 2"));
     this.buttonList.add(new GuiButton(13, this.width / 2 + 5, this.height / 2 - 25, 100, 20, "minecraftjson"));
     this.buttonList.add(new GuiButton(14, this.width / 2 + 5, this.height / 2 - 0, 100, 20, "Back"));
   }
   
   protected void actionPerformed(GuiButton button) throws IOException
   {
     switch (button.id) {
     case 15:
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/potion-generator");

       break;
     case 5: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/tool-generator");
       
       break;
     case 6: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/armor-generator");
       
       break;
     case 7: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/custom-item-generator");
       
       break;
     case 8: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler http://heads.freshcoal.com");
       break;
     case 9: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/command-sign-generator");
       
       break;
     case 10: 
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://minecraftcommand.science/de/prefilled-chest-generator");
       
       break;
         case 11:
             Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler http://mcstacker.bimbimma.com/");
             break;
         case 12:
             Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://mcstacker.net/");
             break;
     case 13:
       Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler https://www.minecraftjson.com/");
       break;
     case 14:
       this.mc.displayGuiScreen(new GuiMyitem());
     }
     
     super.actionPerformed(button);
   }
   
   public void drawScreen(int mouseX, int mouseY, float partialTicks)
   {
     net.minecraft.client.renderer.GlStateManager.color(1.0F, 1.0F, 1.0F);
     drawDefaultBackground();
     super.drawScreen(mouseX, mouseY, partialTicks);
   }
 }

