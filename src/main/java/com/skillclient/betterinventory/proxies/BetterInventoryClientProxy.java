 package com.skillclient.betterinventory.proxies;
 
 import net.minecraft.client.multiplayer.PlayerControllerMP;
 import net.minecraft.entity.player.EntityPlayer;
 import net.minecraft.inventory.ClickType;
 
 public class BetterInventoryClientProxy
   extends BetterInventoryProxy
 {
   public void sendSlotClick(PlayerControllerMP controller, int windowId, int slot, int rightClick, ClickType action, EntityPlayer player)
   {
     controller.windowClick(windowId, slot, rightClick, action, player);
   }
 }

