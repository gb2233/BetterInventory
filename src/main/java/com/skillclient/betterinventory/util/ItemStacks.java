package com.skillclient.betterinventory.util;

import java.io.PrintStream;
import java.util.List;
import java.util.regex.Pattern;

import com.skillclient.betterinventory.Betterinventory;
import com.skillclient.betterinventory.Tabs.TabMyItem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.RegistryNamespaced;

public class ItemStacks
{
  private static String[] args;
  private static Item item;
  private static ItemStack itemstack;
  private static String NBT;
  private static Integer nbtcount = 3;
  static int i = 1;
  static int j = 0;
  static int mode = 0;

  public static ItemStack stringtostack(String Stringargs)
  {
    Stringargs = Stringargs.replace('&', '\u00A7');
    ResourceLocation resourcelocation; for (int mode = 0; mode <= Math.min(Betterinventory.ItemStackModeMax, Stringargs.length() - 2); mode++)
  {
    args = Stringargs.substring(mode).split(Pattern.quote(" "));

    resourcelocation = new ResourceLocation("" + args[0]);
    item = (Item)Item.REGISTRY.getObject(resourcelocation);
    if (item != null) {
      break;
    }
  }
    if (item == null) {
      item = Item.getItemFromBlock(net.minecraft.init.Blocks.BARRIER);
    }
    if (args.length >= 2) {
      i = args[1].matches("[0-9]+") ? Integer.parseInt(args[1]) : 1;
      if ((i < 1) || (i > 64))
        i = 1;
    } else {
      i = 1;
    }
    if (args.length >= 3) {
      j = args[2].matches("[0-9]+") ? Integer.parseInt(args[2]) : 0;
      if ((j < 1) || (j > 64))
        j = 0;
    } else {
      j = 0;
    }



    itemstack = new ItemStack(item, i, j);

    if (args.length >= 4)
    {
      NBT = "";
      for (nbtcount = 3; nbtcount < args.length; nbtcount = nbtcount + 1) {
        NBT = NBT + " " + args[nbtcount];mode = nbtcount;
      }


      try
      {
        itemstack.setTagCompound(JsonToNBT.getTagFromJson(NBT));
      } catch (NBTException nbtexception) {
        System.err.println("Error in NBT Tag");
        System.err.println(NBT);
        System.err.println(nbtexception.getMessage());
      }
    }
    return itemstack;
  }

  public static void saveinv() {
    EntityPlayerSP player = Minecraft.getMinecraft().player;
    for (int faktor = 0; faktor < player.inventory.mainInventory.size(); faktor++)
    {

      if ((player.inventory.mainInventory.toArray()[faktor] != null) &&
              (!TabMyItem.MyItemStacks.contains(player.inventory.mainInventory.toArray()[faktor]))) {
        TabMyItem.MyItemStacks.add(player.inventory.mainInventory.toArray()[faktor]);
      }
    }
  }
}
