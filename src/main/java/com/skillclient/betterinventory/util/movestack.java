 package com.skillclient.betterinventory.util;
 
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.multiplayer.PlayerControllerMP;
 import net.minecraft.entity.player.EntityPlayer;
 import net.minecraft.inventory.ClickType;
 import net.minecraft.inventory.Container;
 import net.minecraft.world.World;
 
 public class movestack
 {
   private static int rightclicknum = 0;
   
   private static void click(int slot, boolean rightclick, ClickType action)
   {
     EntityPlayer player = Minecraft.getMinecraft().player;
     
     World world = player.world;
     
 
     PlayerControllerMP controller = Minecraft.getMinecraft().playerController;
     
     int windowId = player.openContainer.windowId;
     
     if (rightclick) {
       rightclicknum = 1;
     } else {
       rightclicknum = 0;
     }
     com.skillclient.betterinventory.Betterinventory.proxy.sendSlotClick(controller, windowId, slot, rightclicknum, action, player);
   }
   
   private static void drop_stack(int slot, boolean fullstack) {
     click(slot, fullstack, ClickType.THROW);
   }
   
   public static void drop_everything(int slot_from, int slot_to)
   {
     EntityPlayer player = Minecraft.getMinecraft().player;
     for (int slot = slot_from; slot <= slot_to; slot++) {
         drop_stack(slot, true);
         Chat.chat(Integer.toString(slot));
     }
   }
   
   public static void drop_allitem(int slot_from, int slot_to)
   {
     EntityPlayer player = Minecraft.getMinecraft().player;
     for (int slot = slot_from; slot <= slot_to; slot++) {
       drop_stack(slot, true);
     }
     Chat.chat("KeyPressed");
   }
 }

