 package com.skillclient.betterinventory.util;
 
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.settings.KeyBinding;
 import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
 import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
 
 public class KeyHandler
 {
   @SubscribeEvent
   public void onKeypressed(InputEvent.KeyInputEvent event)
   {
     if (com.skillclient.betterinventory.Betterinventory.betterinventory_gui.isKeyDown()) {
       Minecraft.getMinecraft().displayGuiScreen(new com.skillclient.betterinventory.Gui.GuiMyitem());
     }
   }
 }

