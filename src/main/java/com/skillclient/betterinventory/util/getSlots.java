 package com.skillclient.betterinventory.util;
 
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.gui.GuiMerchant;
 import net.minecraft.client.gui.GuiRepair;
 import net.minecraft.client.gui.GuiScreen;
 import net.minecraft.client.gui.inventory.GuiBeacon;
 import net.minecraft.client.gui.inventory.GuiBrewingStand;
 import net.minecraft.client.gui.inventory.GuiChest;
 import net.minecraft.client.gui.inventory.GuiCrafting;
 import net.minecraft.client.gui.inventory.GuiDispenser;
 import net.minecraft.client.gui.inventory.GuiFurnace;
 import net.minecraft.client.gui.inventory.GuiInventory;
 import net.minecraft.client.gui.inventory.GuiScreenHorseInventory;
 
 public class getSlots
 {
   public static int[] inventoryslots1()
   {
     GuiScreen screen = Minecraft.getMinecraft().currentScreen;
     
 
     int[] slots = { 9, 44, 0, 1, 4, 5, 8 };
     if ((screen instanceof GuiInventory))
       slots = new int[] { 9, 44, 0, 0, 1, 4, 5, 8 };
     if ((screen instanceof GuiChest))
       slots = new int[] { 27, 62, 0, 26, 0, 26, 0, 26 };
     if ((screen instanceof GuiCrafting))
       slots = new int[] { 10, 45, 0, 0, 1, 9, 1, 9 };
     if ((screen instanceof GuiFurnace))
       slots = new int[] { 3, 38, 2, 2, 0, 0, 1, 1 };
     if ((screen instanceof GuiDispenser))
       slots = new int[0];
     return slots;
   }
   
   public static int[] inventoryslots() {
     GuiScreen screen = Minecraft.getMinecraft().currentScreen;
     
     int[] slots = { 0, 44 };
     if ((screen instanceof GuiInventory))
       slots = new int[] { 0, 44 };
     if ((screen instanceof GuiChest))
       slots = new int[] { 0, 62 };
     if ((screen instanceof GuiCrafting))
       slots = new int[] { 0, 45 };
     if ((screen instanceof GuiFurnace))
       slots = new int[] { 0, 38 };
     if ((screen instanceof GuiDispenser))
       slots = new int[] { 0, 44 };
     if ((screen instanceof net.minecraft.client.gui.GuiEnchantment))
       slots = new int[] { 0, 37 };
     if ((screen instanceof GuiBrewingStand))
       slots = new int[] { 0, 39 };
     if ((screen instanceof GuiMerchant))
       slots = new int[] { 0, 38 };
     if ((screen instanceof GuiBeacon))
       slots = new int[] { 0, 36 };
     if ((screen instanceof GuiRepair))
       slots = new int[] { 0, 38 };
     if ((screen instanceof net.minecraft.client.gui.GuiHopper))
       slots = new int[] { 0, 40 };
     if ((screen instanceof GuiScreenHorseInventory)) {
       slots = new int[] { 0, 37 };
     }
     return slots;
   }
 }

