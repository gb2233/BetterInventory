 package com.skillclient.betterinventory.util;
 
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.gui.GuiIngame;
 import net.minecraft.client.gui.GuiNewChat;
 import net.minecraft.util.text.translation.I18n;
 
 public class Chat
 {
   public static void chat(String message)
   {
     System.out.println(message);
     Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(new net.minecraft.util.text.TextComponentString(message));
   }
   
   public static String translate(String raw) {
     return I18n.translateToLocal(raw);
   }
   
   public static String black = "\u00A7\u0030";
   public static String dark_blue = "\u00A7\u0031";
   public static String dark_green = "\u00A7\u0032";
   public static String dark_aqua = "\u00A7\u0033";
   public static String dark_red = "\u00A7\u0034";
   public static String dark_purple = "\u00A7\u0035";
   public static String gold = "\u00A7\u0036";
   public static String gray = "\u00A7\u0037";
   public static String dark_gray = "\u00A7\u0038";
   public static String blue = "\u00A7\u0039";
   public static String green = "\u00A7\u0061";
   public static String aqua = "\u00A7\u0062";
   public static String red = "\u00A7\u0063";
   public static String light_purple = "\u00A7\u0064";
   public static String yellow = "\u00A7\u0065";
   public static String white = "\u00A7\u0066";
   public static String Obfuscated = "\u00A7\u006B";
   public static String Bold = "\u00A7\u006C";
   public static String Strikethrough = "\u00A7\u006D";
   public static String Underline = "\u00A7\u006E";
   public static String Italic = "\u00A7\u006F";
   public static String Reset = "\u00A7\u0072";
   
   public static String code(int a) {
     return "\u00A7" + a;
   }
   
   public static String code(String a) {
     return "\u00A7" + a;
   }
 }

