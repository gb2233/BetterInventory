 package com.skillclient.betterinventory.util;
 
 import java.io.File;
 import java.io.FileOutputStream;
 import java.io.FileWriter;
 import java.io.IOException;
 import java.net.URL;
 import java.nio.channels.Channels;
 import java.nio.channels.ReadableByteChannel;
 import java.util.ArrayList;
 import java.util.List;
 import java.util.Scanner;

import com.skillclient.betterinventory.Betterinventory;
import com.skillclient.betterinventory.Tabs.TabMyItem;

 import net.minecraft.item.ItemStack;
 
 
 public class Download
 {
     //private static final String UPDATE_URL = "https://www.dropbox.com/s/o9wz9h6y1eqfco0/info.txt?dl=1";
   private static boolean newVersionAvailable = false;
   
   public static String latest_Version;
   
   public static String latest_Version_id;
   public static String download;
   public static int Version_difference;
    private static final String URL_OldPlayerHeadsStacks = "https://www.dropbox.com/s/grggmygdvl7n0pw/OldPlayerHeadsStacks.txt?dl=1";
    private static final String URL_PlayerHeadsStacks = "https://www.dropbox.com/s/qelhegcgmuzhtkj/PlayerHeadsStacks.txt?dl=1";
    private static final String URL_AlphabetHeadsStacks = "https://www.dropbox.com/s/9asyiugp8k9xgce/AlphabetHeadsStacks.txt?dl=1";
    private static final String URL_AnimalHeadsStacks = "https://www.dropbox.com/s/vjw2l94g2rwjmmp/AnimalHeadsStacks.txt?dl=1";
    private static final String URL_BlockHeadsStacks = "https://www.dropbox.com/s/z3vgeb604hmrmvz/BlockHeadsStacks.txt?dl=1";
    private static final String URL_DecorationHeadsStacks = "https://www.dropbox.com/s/jxc8lpqwkkmszy0/DecorationHeadsStacks.txt?dl=1";
    private static final String URL_FoodHeadsStacks = "https://www.dropbox.com/s/33bt5pg77glhesr/FoodHeadsStacks.txt?dl=1";
    private static final String URL_HumanHeadsStacks = "https://www.dropbox.com/s/g92ei3esck6aorm/HumanHeadsStacks.txt?dl=1";
    private static final String URL_HumanoidHeadsStacks = "https://www.dropbox.com/s/1bku671inxc3ft9/HumanoidHeadsStacks.txt?dl=1";
    private static final String URL_MiscHeadsStacks = "https://www.dropbox.com/s/5q6jupkt3t0rsvg/MiscHeadsStacks.txt?dl=1";
    private static final String URL_MonsterHeadsStacks = "https://www.dropbox.com/s/smp5cpeyb5es4cn/MonsterHeadsStacks.txt?dl=1";
    private static final String URL_PlantHeadsStacks = "https://www.dropbox.com/s/tpsq5vab0o7jfk2/PlantHeadsStacks.txt?dl=1";
   public static List<ItemStack> OldPlayerHeadsStacks = new ArrayList<>();
   public static List<ItemStack> PlayerHeadsStacks = new ArrayList<>();
   public static List<ItemStack> AlphabetHeadsStacks = new ArrayList<>();
   public static List<ItemStack> AnimalHeadsStacks = new ArrayList<>();
   public static List<ItemStack> BlockHeadsStacks = new ArrayList<>();
   public static List<ItemStack> DecorationHeadsStacks = new ArrayList<>();
   public static List<ItemStack> FoodHeadsStacks = new ArrayList<>();
   public static List<ItemStack> HumanHeadsStacks = new ArrayList<>();
   public static List<ItemStack> HumanoidHeadsStacks = new ArrayList<>();
   public static List<ItemStack> MiscHeadsStacks = new ArrayList<>();
   public static List<ItemStack> MonsterHeadsStacks = new ArrayList<>();
   public static List<ItemStack> PlantHeadsStacks = new ArrayList<>();
   
   //private static final String URL_SPAWNER = "https://www.dropbox.com/s/b0ur1o9fx2svlw5/Spawner.txt?dl=1";
   
   //public static List<ItemStack> SpawnerStacks = new ArrayList<>();
   
   private static final String URl_youtube = "https://www.dropbox.com/s/13a46pz6tvsltvq/Youtube.txt?dl=1";
   
   public static List<ItemStack> YoutubeStacks = new ArrayList<>();


     private static final File Folder = new File("mods/BetterInv");
     private static final File file = new File("mods/BetterInv/BetterInventory-MyItem.txt");
   private static final File UPDATE_FILE = new File("mods/BetterInv/info.txt");
   private static final File FILE_OldPlayerHeadsStacks = new File("mods/BetterInv/OldPlayerHeadsStacks.txt");
   private static final File FILE_PlayerHeadsStacks = new File("mods/BetterInv/PlayerHeadsStacks.txt");
   private static final File FILE_AlphabetHeadsStacks = new File("mods/BetterInv/AlphabetHeadsStacks.txt");
   private static final File FILE_AnimalHeadsStacks = new File("mods/BetterInv/AnimalHeadsStacks.txt");
   private static final File FILE_BlockHeadsStacks = new File("mods/BetterInv/BlockHeadsStacks.txt");
   private static final File FILE_DecorationHeadsStacks = new File("mods/BetterInv/DecorationHeadsStacks.txt");
   private static final File FILE_FoodHeadsStacks = new File("mods/BetterInv/FoodHeadsStacks.txt");
   private static final File FILE_HumanHeadsStacks = new File("mods/BetterInv/HumanHeadsStacks.txt");
   private static final File FILE_HumanoidHeadsStacks = new File("mods/BetterInv/HumanoidHeadsStacks.txt");
   private static final File FILE_MiscHeadsStacks = new File("mods/BetterInv/MiscHeadsStacks.txt");
   private static final File FILE_MonsterHeadsStacks = new File("mods/BetterInv/MonsterHeadsStacks.txt");
   private static final File FILE_PlantHeadsStacks = new File("mods/BetterInv/PlantHeadsStacks.txt");

   //private static final File FILE_SPAWNER = new File("mods/BetterInv/Spawner.txt");
   private static final File FILE_youtube = new File("mods/BetterInv/Youtube.txt");

   public static void start()
   {
       if(!Folder.exists())
           Folder.mkdirs();
     /*try {
       Scanner scanner = new Scanner(UPDATE_FILE);
       latest_Version = scanner.nextLine();
       latest_Version_id = scanner.nextLine();
       download = scanner.nextLine();
       scanner.close();
       Version_difference = Integer.parseInt(latest_Version_id) - 1;
       if (Version_difference > 0) {
         setnewVersionAvailable();
       }
     } catch (IOException e) {
       e.printStackTrace();
     }*/


       String text;
       if (Betterinventory.tabs_OldPlayerHeads) {
           try
           {
               if (!FILE_OldPlayerHeadsStacks.exists()){
                   URL url = new URL(URL_OldPlayerHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_OldPlayerHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }

               Scanner scanner = new Scanner(FILE_OldPlayerHeadsStacks);
               scanner.nextLine();
               OldPlayerHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   OldPlayerHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_PlayerHeads) {
           try
           {
               if (!FILE_PlayerHeadsStacks.exists()){
                   URL url = new URL(URL_PlayerHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_PlayerHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_PlayerHeadsStacks);
               scanner.nextLine();
               PlayerHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   PlayerHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_AlphabetPlayerHeads) {
           try
           {
               if (!FILE_AlphabetHeadsStacks.exists()){
                   URL url = new URL(URL_AlphabetHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_AlphabetHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_AlphabetHeadsStacks);
               scanner.nextLine();
               AlphabetHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   AlphabetHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_AnimalPlayerHeads) {
           try
           {
               if (!FILE_AnimalHeadsStacks.exists()){
                   URL url = new URL(URL_AnimalHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_AnimalHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_AnimalHeadsStacks);
               scanner.nextLine();
               AnimalHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   AnimalHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_BlockPlayerHeads) {
           try
           {
               if (!FILE_BlockHeadsStacks.exists()){
                   URL url = new URL(URL_BlockHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_BlockHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_BlockHeadsStacks);
               scanner.nextLine();
               BlockHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   BlockHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_DecorationPlayerHeads) {
           try
           {
               if (!FILE_DecorationHeadsStacks.exists()){
                   URL url = new URL(URL_DecorationHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_DecorationHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_DecorationHeadsStacks);
               scanner.nextLine();
               DecorationHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   DecorationHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_FoodPlayerHeads) {
           try
           {
               if (!FILE_FoodHeadsStacks.exists()){
                   URL url = new URL(URL_FoodHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_FoodHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_FoodHeadsStacks);
               scanner.nextLine();
               FoodHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   FoodHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_HumanPlayerHeads) {
           try
           {
               if (!FILE_HumanHeadsStacks.exists()){
                   URL url = new URL(URL_HumanHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_HumanHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_HumanHeadsStacks);
               scanner.nextLine();
               HumanHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   HumanHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_HumanoidPlayerHeads) {
           try
           {
               if (!FILE_HumanoidHeadsStacks.exists()){
                   URL url = new URL(URL_HumanoidHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_HumanoidHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_HumanoidHeadsStacks);
               scanner.nextLine();
               HumanoidHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   HumanoidHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_MiscPlayerHeads) {
           try
           {
               if (!FILE_MiscHeadsStacks.exists()){
                   URL url = new URL(URL_MiscHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_MiscHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_MiscHeadsStacks);
               scanner.nextLine();
               MiscHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   MiscHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_MonsterPlayerHeads) {
           try
           {
               if (!FILE_MonsterHeadsStacks.exists()){
                   URL url = new URL(URL_MonsterHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_MonsterHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_MonsterHeadsStacks);
               scanner.nextLine();
               MonsterHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   MonsterHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       if (Betterinventory.tabs_PlantPlayerHeads) {
           try
           {
               if (!FILE_PlantHeadsStacks.exists()){
                   URL url = new URL(URL_PlantHeadsStacks);
                   ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                   FileOutputStream fos = new FileOutputStream(FILE_PlantHeadsStacks);
                   fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
               }
               Scanner scanner = new Scanner(FILE_PlantHeadsStacks);
               scanner.nextLine();
               PlantHeadsStacks.clear();
               while (scanner.hasNextLine()) {
                   text = scanner.nextLine();
                   PlantHeadsStacks.add(ItemStacks.stringtostack("minecraft:skull 1 3 " + text));
               }
               scanner.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }

     /*try
     {
         if (!FILE_SPAWNER.exists()){
             URL url = new URL(URL_SPAWNER);
             ReadableByteChannel rbc = Channels.newChannel(url.openStream());
             FileOutputStream fos = new FileOutputStream(FILE_SPAWNER);
             fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
         }
       Scanner scanner = new Scanner(FILE_SPAWNER);
       SpawnerStacks.clear();
       while (scanner.hasNextLine()) {
         text = scanner.nextLine();
         if ((text.length() >= 2) && (!"//".equals(text.substring(0, 2)))) {
           SpawnerStacks.add(ItemStacks.stringtostack("minecraft:mob_spawner 1 0 " + text));
         }
       }
       
       scanner.close();
     } catch (IOException e) {
       e.printStackTrace();
     }*/
     
     try
     {
         if (!FILE_youtube.exists()){
             URL url = new URL(URl_youtube);
             ReadableByteChannel rbc = Channels.newChannel(url.openStream());
             FileOutputStream fos = new FileOutputStream(FILE_youtube);
             fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
         }
       Scanner scanner = new Scanner(FILE_youtube);
       YoutubeStacks.clear();
       while (scanner.hasNextLine()) {
         text = scanner.nextLine();
         if ((text.length() >= 2) && 
           (!"//".equals(text.substring(0, 2)))) {
           YoutubeStacks.add(ItemStacks.stringtostack(text));
         }
       }
       
       scanner.close();
     } catch (IOException e) {
       e.printStackTrace();
     }
     
 
     try
     {
       if (!file.exists()) {
         file.createNewFile();
         FileWriter writer = new FileWriter(file);
         writer.write("// ############################################################\n");
         writer.write("// #                                                          #\n");
         writer.write("// #  In this file you can add ItemStacks to the MyItem-Tab.  #\n");
         writer.write("// #                                                          #\n");
         writer.write("// ############################################################\n");
         writer.write("//\n");
         writer.write("minecraft:written_book 1 0 {pages:[\"[\\\"\\\",{\\\"text\\\":\\\"Hi \\\"},{\\\"selector\\\":\\\"@p\\\"},{\\\"text\\\":\\\"\\\\nTo use this Tab, you can use the Config-File in the Config-Folder of your Minecraft-Path\\\\nOr in the Gui, which can be accessed by Pressing \\\"},{\\\"text\\\":\\\"-\\\",\\\"color\\\":\\\"red\\\"},{\\\"text\\\":\\\" on your NumPad or another Key, defined in the Controls\\\\nIn the Gui there's a button called '\\\",\\\"color\\\":\\\"reset\\\"},{\\\"text\\\":\\\"Save Inventory\\\",\\\"italic\\\":true,\\\"color\\\":\\\"red\\\"},{\\\"text\\\":\\\"', to \\\",\\\"color\\\":\\\"reset\\\"}]\",\"[\\\"\\\",{\\\"text\\\":\\\"save your current Inventory AND a button called '\\\"},{\\\"text\\\":\\\"MyItem\\\",\\\"bold\\\":true,\\\"color\\\":\\\"aqua\\\"},{\\\"text\\\":\\\"', which can be used like the give-command but with a better Syntax.\\\",\\\"color\\\":\\\"reset\\\"}]\"],title:\"My-Items\",author:gbalint,generation:3,display:{Lore:[\"Example book\"]}}");
           writer.write("\n");
         writer.write("minecraft:potion 1 16395 {CustomPotionEffects:[{Id:11,Amplifier:5,Duration:2000000}],display:{Name:\"Potion: Resistance\"}}");
           writer.write("\n");
         writer.write("minecraft:chest 1 0 {BlockEntityTag:{Items:[{id:368,Slot:13,Count:64},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block\"}},Slot:18},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:19},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:20},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:21},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond, Bitch!\"}},Slot:22},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:23},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:24},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:25},{id:\"diamond_block\",Count:1,tag:{display:{Name:\"Diamond Block!\"}},Slot:26}]},display:{Name:\"Nice Chest\"}}");
           writer.write("\n");
         writer.write("minecraft:diamond_axe 1 0 {AttributeModifiers:[{AttributeName:\"generic.attackDamage\",Name:\"generic.attackDamage\",Amount:-1,Operation:0,UUIDMost:95037,UUIDLeast:891847}]}");
           writer.write("\n");
         writer.write("minecraft:sign 1 0 {BlockEntityTag:{Text1:\"[\\\"\\\",{\\\"text\\\":\\\"Hallo \\\",\\\"color\\\":\\\"blue\\\"}]\",Text2:\"[\\\"\\\",{\\\"selector\\\":\\\"@a\\\",\\\"color\\\":\\\"green\\\",\\\"bold\\\":true}]\",id:\"Sign\"}}");
           writer.write("\n");
         writer.write("minecraft:iron_ingot\n");
         
         writer.close();
       }
       Scanner scanner = new Scanner(file);
       TabMyItem.MyItemStacks.clear();
       while (scanner.hasNextLine()) {
         text = scanner.nextLine();
         if ((text.length() >= 2) && 
           (!"//".equals(text.substring(0, 2)))) {
           TabMyItem.MyItemStacks.add(ItemStacks.stringtostack(text));
         }
       }
       
       scanner.close();
     } catch (IOException e) {
       e.printStackTrace();
     }
   }

   private static synchronized void setnewVersionAvailable()
   {
     newVersionAvailable = true;
   }
   
   public static synchronized boolean isnewVersionAvailable() {
     return newVersionAvailable;
   }
 }
