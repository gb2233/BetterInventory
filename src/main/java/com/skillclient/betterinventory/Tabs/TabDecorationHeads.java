 package com.skillclient.betterinventory.Tabs;

 import com.skillclient.betterinventory.util.Download;
 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;

 import java.time.LocalDate;

 public class TabDecorationHeads extends CreativeTabs
 {
   ItemStack empty;
   public TabDecorationHeads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
       return ItemStacks.stringtostack("minecraft:skull 1 3 {display:{Name:\"Paint Bucket (rainbow)\"},SkullOwner:{Id:\"70aa5f36-499f-4981-a63f-9265316324d2\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTlhMjIwNTU1NGY0ZmQ4NDMxY2NhMzRkYTlmNGQ5ODYyMDU4NDg4YmJjNGIwZDY3MTk0MzJkMTEwYzUzYTA3ZCJ9fX0=\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.DecorationHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
