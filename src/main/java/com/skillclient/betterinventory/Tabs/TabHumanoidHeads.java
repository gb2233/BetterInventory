 package com.skillclient.betterinventory.Tabs;

 import com.skillclient.betterinventory.util.Download;
 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;

 import java.time.LocalDate;

 public class TabHumanoidHeads extends CreativeTabs
 {
   ItemStack empty;
   public TabHumanoidHeads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
       return ItemStacks.stringtostack("minecraft:skull 1 3 {display:{Name:\"Robot (blue)\"},SkullOwner:{Id:\"f87990dc-ca4a-4d66-83ce-bd35835e958d\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZmNzI4Yzg5OTA0YjJjYjU3Zjg1M2QzMWQwZTIwNjFmNTI5MTc5ODFmZWRjY2IxZTk0OTUyOGUwOGViNDE0MCJ9fX0=\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.HumanoidHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
