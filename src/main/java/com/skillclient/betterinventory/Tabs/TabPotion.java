 package com.skillclient.betterinventory.Tabs;
 
 import java.util.List;

import com.skillclient.betterinventory.util.ItemStacks;

 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.Item;
 import net.minecraft.item.ItemStack;
 import net.minecraft.nbt.JsonToNBT;
 import net.minecraft.nbt.NBTException;
 import net.minecraft.potion.Potion;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.util.Constants.NBT;
 
 public class TabPotion
   extends CreativeTabs
 {
   public TabPotion(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.POTIONITEM);
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     String[] array = { "{AttributeModifiers:[{AttributeName:\"generic.maxHealth\",Name:\"generic.maxHealth\",Amount:20,Operation:0,UUIDLeast:967858,UUIDMost:899140}]}", "{AttributeModifiers:[{AttributeName:\"generic.maxHealth\",Name:\"generic.maxHealth\",Amount:-20,Operation:0,UUIDLeast:967858,UUIDMost:899140}]}", "{AttributeModifiers:[{AttributeName:\"generic.movementSpeed\",Name:\"generic.movementSpeed\",Amount:0.1,Operation:1,UUIDLeast:15939,UUIDMost:395317}]}", "{AttributeModifiers:[{AttributeName:\"generic.movementSpeed\",Name:\"generic.movementSpeed\",Amount:1,Operation:1,UUIDLeast:15939,UUIDMost:395317}]}", "{AttributeModifiers:[{AttributeName:\"generic.movementSpeed\",Name:\"generic.movementSpeed\",Amount:10,Operation:1,UUIDLeast:15939,UUIDMost:395317}]}" };
     
     for (String NBT : array) {
       ItemStack stick = new ItemStack(Items.STICK);
       try {
         stick.setTagCompound(JsonToNBT.getTagFromJson(NBT));
       }
       catch (NBTException localNBTException) {}
       p_78018_1_.add(stick);
     }
     p_78018_1_.add(new ItemStack(Items.MILK_BUCKET));
     p_78018_1_.add(new ItemStack(Items.AIR));
     p_78018_1_.add(new ItemStack(Items.AIR));
     p_78018_1_.add(new ItemStack(Items.AIR));
     String[] types = { "potion", "splash_potion", "lingering_potion" };
     String[] amplifiers = { "0", "1", "2", "12", "60", "144", "254", "255" };
     int[] durations = { 1, 20, 60, 100000 };
     String[] NBT = types;
     int stick_nbt = NBT.length;
     for (int stick = 0; stick < stick_nbt; stick++) {
       String type = NBT[stick];
       for (int effect = 1; effect <= 25; effect++) {
         for (String amplifier : amplifiers) {
           for (int duration : durations) {
               try {
                   p_78018_1_.add(ItemStacks.stringtostack(type + " 1 0 {CustomPotionEffects:[{Id:" + effect + ",Amplifier:" + amplifier + ",Duration:" + duration * 20 + "}],display:{Name:\"" + type + " " + 
                   Potion.getPotionById(effect).getName() + " " + amplifier + " for " + (duration == 100000 ? "ever" : new StringBuilder().append(duration).append("sec").toString()) + "\"}}"));
               }
               catch (Exception localNBTException) {
            	   System.err.println(localNBTException.getMessage());
                   p_78018_1_.add(ItemStacks.stringtostack("potion"));
               }
           }
         }
       }
     }
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }

