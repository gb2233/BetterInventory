 package com.skillclient.betterinventory.Tabs;
 
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.nbt.JsonToNBT;
 import net.minecraft.nbt.NBTException;
import net.minecraft.util.NonNullList;
 
 public class TabArmor
   extends CreativeTabs
 {
   ItemStack empty;
   
   public TabArmor(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
     this.setNoScrollbar();
   }
   
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.DIAMOND_CHESTPLATE);
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     String[] array = { "{Unbreakable:1,ench:[{id:0,lvl:1000}]}", "{AttributeModifiers:[{AttributeName:\"generic.knockbackResistance\",Name:\"generic.knockbackResistance\",Amount:1,Operation:0,UUIDLeast:722576,UUIDMost:658559,Slot:\"head\"}],Unbreakable:1,ench:[{id:0,lvl:1000}]}", "{Unbreakable:1,ench:[{id:0,lvl:1000},{id:7,lvl:1000}]}", "{AttributeModifiers:[{AttributeName:\"generic.knockbackResistance\",Name:\"generic.knockbackResistance\",Amount:1,Operation:0,UUIDLeast:859071,UUIDMost:670308}],Unbreakable:1,ench:[{id:0,lvl:1000},{id:7,lvl:1000}]}" };
     
 
 
     for (String NBT : array) {
       ItemStack diamond_helmet = new ItemStack(Items.DIAMOND_HELMET);
       ItemStack diamond_chestplate = new ItemStack(Items.DIAMOND_CHESTPLATE);
       ItemStack diamond_leggings = new ItemStack(Items.DIAMOND_LEGGINGS);
       ItemStack diamond_boots = new ItemStack(Items.DIAMOND_BOOTS);
       ItemStack[] array1 = { diamond_helmet, diamond_chestplate, diamond_leggings, diamond_boots };
       
       for (ItemStack stack : array1) {
         try {
           stack.setTagCompound(JsonToNBT.getTagFromJson(NBT));
         }
         catch (NBTException localNBTException) {}
         p_78018_1_.add(stack);
       }
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
     }
   }
   
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }

