 package com.skillclient.betterinventory.Tabs;
 
 import com.skillclient.betterinventory.util.ItemStacks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;
 
 public class TabBlocks
   extends CreativeTabs
 {
   ItemStack empty;
   
   public TabBlocks(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
     this.setNoScrollbar();
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Item.getItemFromBlock(Blocks.DRAGON_EGG));
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.add(new ItemStack(Blocks.COMMAND_BLOCK));
     p_78018_1_.add(new ItemStack(Blocks.CHAIN_COMMAND_BLOCK));
     p_78018_1_.add(new ItemStack(Blocks.REPEATING_COMMAND_BLOCK));
     p_78018_1_.add(new ItemStack(Blocks.DRAGON_EGG));
     p_78018_1_.add(new ItemStack(Items.COMMAND_BLOCK_MINECART));
     p_78018_1_.add(new ItemStack(Blocks.BARRIER));
     p_78018_1_.add(new ItemStack(Items.WRITTEN_BOOK).setStackDisplayName("This Book is empty. Use the 'MyItem'-tab instead."));
     p_78018_1_.add(new ItemStack(Items.FILLED_MAP));
     p_78018_1_.add(new ItemStack(Blocks.FARMLAND));p_78018_1_.add(new ItemStack(Blocks.RED_MUSHROOM_BLOCK));
     p_78018_1_.add(new ItemStack(Blocks.BROWN_MUSHROOM_BLOCK));
     p_78018_1_.add(ItemStacks.stringtostack("minecraft:banner 1 0 {BlockEntityTag:{Base:4,Patterns:[{Pattern:gra,Color:1},{Pattern:bri,Color:10},{Pattern:cbo,Color:10}]}}"));

     p_78018_1_.add(ItemStacks.stringtostack("minecraft:shield 1 0 {BlockEntityTag:{Base:4,Patterns:[{Pattern:gra,Color:1},{Pattern:bri,Color:10},{Pattern:cbo,Color:10}]}}"));
     
     p_78018_1_.add(new ItemStack(Blocks.GRASS_PATH));
     
     p_78018_1_.add(ItemStacks.stringtostack("minecraft:pumpkin 1 0 {HideFlags:1,display:{Name:\"Ender Helmet\",Lore:[\"Trolololo\"]},ench:[{id:10,lvl:1}]}"));
   }
   
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }