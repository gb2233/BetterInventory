 package com.skillclient.betterinventory.Tabs;
 
 import java.util.List;

import com.skillclient.betterinventory.util.ItemStacks;

 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.Item;
 import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class TabSpawneggs
   extends CreativeTabs
 {
	 ItemStack empty;
   public TabSpawneggs(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }
   
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SPAWN_EGG);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     String[] array = { "chests/abandoned_mineshaft", "chests/desert_pyramid", "chests/end_city_treasure", "chests/igloo_chest", "chests/jungle_temple", "chests/nether_bridge", "chests/simple_dungeon", "chests/spawn_bonus_chest", "chests/stronghold_corridor", "chests/stronghold_crossing", "chests/stronghold_library", "chests/village_blacksmith", "entities/sheep/black", "entities/sheep/blue", "entities/sheep/cyan", "entities/sheep/green", "entities/sheep/lime", "entities/sheep/orange", "entities/sheep/pink", "entities/sheep/purple", "entities/sheep/red", "entities/sheep/silver", "entities/sheep/white", "entities/sheep/yellow", "entities/bat", "entities/blaze", "entities/cave_spider", "entities/chicken", "entities/cow", "entities/creeper", "entities/enderman", "entities/ghast", "entities/giant", "entities/guardian", "entities/horse", "entities/iron_golem", "entities/magma_cube", "entities/mushroom_cow", "entities/ocelot", "entities/pig", "entities/sheep", "entities/shulker", "entities/skeleton", "entities/skeleton_horse", "entities/snowman", "entities/spider", "entities/squid", "entities/witch", "entities/wither_skeleton", "entities/wolf", "entities/zombie", "entities/zombie_horse", "entities/zombie_pigman", "gameplay/fishing", "gameplay/fishing/fish", "gameplay/fishing/junk", "gameplay/fishing/treasure", "empty" };
     
     for (String name : array) {
       p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Loottable: " + name + "\"},EntityTag:{id:Bat,DeathLootTable:\"" + name + "\",ActiveEffects:[{Id:7,Amplifier:0,Duration:2147483647}]}}"));
     }
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Charged Creeper\"},EntityTag:{id:Creeper,powered:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Wither Skeleton\"},EntityTag:{id:\"minecraft:wither_skeleton\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 0\"},EntityTag:{id:Slime,Size:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 1\"},EntityTag:{id:Slime,Size:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 2\"},EntityTag:{id:Slime,Size:2}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 3\"},EntityTag:{id:Slime,Size:3}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 10\"},EntityTag:{id:Slime,Size:10}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 100\"},EntityTag:{id:Slime,Size:100}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Slime 1000\"},EntityTag:{id:Slime,Size:1000}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 0\"},EntityTag:{id:\"minecraft:magma_cube\",Size:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 1\"},EntityTag:{id:\"minecraft:magma_cube\",Size:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 2\"},EntityTag:{id:\"minecraft:magma_cube\",Size:2}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 3\"},EntityTag:{id:\"minecraft:magma_cube\",Size:3}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 10\"},EntityTag:{id:\"minecraft:magma_cube\",Size:10}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 100\"},EntityTag:{id:\"minecraft:magma_cube\",Size:100}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"LavaSlime 1000\"},EntityTag:{id:\"minecraft:magma_cube\",Size:1000}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Elder Guardian\"},EntityTag:{id:\"minecraft:elder_guardian\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Angry Wolf\"},EntityTag:{id:\"minecraft:wolf\",Angry:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Ozelot Tuxedo\"},EntityTag:{id:\"minecraft:ocelot\",CatType:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Ozelot Tabby\"},EntityTag:{id:\"minecraft:ocelot\",CatType:2}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Ozelot Siamese\"},EntityTag:{id:\"minecraft:ocelot\",CatType:3}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Horse\"},EntityTag:{id:\"minecraft:horse\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Donkey\"},EntityTag:{id:\"minecraft:donkey\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Mule\"},EntityTag:{id:\"minecraft:mule\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Zombie Horse\"},EntityTag:{id:\"minecraft:zombie_horse\"}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Skeleton Horse\"},EntityTag:{id:\"minecraft:skeleton_horse\"}}"));
	 p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Killer Rabbit\"},EntityTag:{id:\"minecraft:rabbit\",RabbitType:99}}"));
     /*p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Fletcher\"},EntityTag:{id:\"minecraft:villager\",Profession:0,Career:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Farmer\"},EntityTag:{id:\"minecraft:villager\",Profession:0,Career:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Fisherman\"},EntityTag:{id:\"minecraft:villager\",Profession:0,Career:2}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Shepherd\"},EntityTag:{id:\"minecraft:villager\",Profession:0,Career:3}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Librarian\"},EntityTag:{id:\"minecraft:villager\",Profession:1,Career:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Cleric\"},EntityTag:{id:\"minecraft:villager\",Profession:2,Career:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Tool Smith\"},EntityTag:{id:\"minecraft:villager\",Profession:3,Career:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Armorer\"},EntityTag:{id:\"minecraft:villager\",Profession:3,Career:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Weapon Smith\"},EntityTag:{id:\"minecraft:villager\",Profession:3,Career:2}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Leatherworker\"},EntityTag:{id:\"minecraft:villager\",Profession:4,Career:0}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Butcher\"},EntityTag:{id:\"minecraft:villager\",Profession:4,Career:1}}"));
     p_78018_1_.add(ItemStacks.stringtostack("spawn_egg 1 0 {display:{Name:\"Villager Nitwit\"},EntityTag:{id:\"minecraft:villager\",Profession:5,Career:0}}"));*/
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
