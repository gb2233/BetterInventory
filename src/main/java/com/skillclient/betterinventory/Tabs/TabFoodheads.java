 package com.skillclient.betterinventory.Tabs;

 import com.skillclient.betterinventory.util.Download;
 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;

 import java.time.LocalDate;

 public class TabFoodheads extends CreativeTabs
 {
   ItemStack empty;
   public TabFoodheads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
       return ItemStacks.stringtostack("minecraft:skull 1 3 {display:{Name:\"Medieval Beer\"},SkullOwner:{Id:\"aa6a2406-1033-471e-943b-8939ee92fe6a\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDg0M2UwNmZkNGU2ZTU2ZDI1NTRlYTJkZGU4Mzc4M2I4MTY3NWZlYjI0YjE2N2ZlOThiMzgyN2U0ZjYyNTQ0NiJ9fX0=\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.FoodHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
