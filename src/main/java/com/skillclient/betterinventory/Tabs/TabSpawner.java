 package com.skillclient.betterinventory.Tabs;
 
 import java.util.List;

import com.skillclient.betterinventory.util.Download;

 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Blocks;
 import net.minecraft.item.Item;
 import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class TabSpawner
   extends CreativeTabs
 {
	 ItemStack empty;
   public TabSpawner(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }
   
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Item.getItemFromBlock(Blocks.MOB_SPAWNER));
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
       p_78018_1_.add(ItemStacks.stringtostack("minecraft:paper 1 0 {display:{Name:\"Disabled\",Lore:[\"Spawners don't work\",\" even in single\"]}}"));
     //p_78018_1_.addAll(Download.SpawnerStacks);
   }

   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }

