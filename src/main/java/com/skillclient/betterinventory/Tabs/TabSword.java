 package com.skillclient.betterinventory.Tabs;
 
 import java.util.List;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.Item;
 import net.minecraft.item.ItemStack;
 import net.minecraft.nbt.JsonToNBT;
 import net.minecraft.nbt.NBTException;
import net.minecraft.util.NonNullList;
 
 public class TabSword
   extends CreativeTabs
 {
   ItemStack empty;
   
   public TabSword(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
     this.setNoScrollbar();
   }
   
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.DIAMOND_SWORD);
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     String[] array = { "{Unbreakable:1,ench:[{id:16,lvl:1000}]}", "{Unbreakable:1,ench:[{id:21,lvl:1000}]}", "{Unbreakable:1,ench:[{id:16,lvl:1000},{id:22,lvl:5}]}", "{Unbreakable:1,ench:[{id:16,lvl:1000},{id:21,lvl:1000}]}", "{Unbreakable:1,ench:[{id:19,lvl:5}]}", "{Unbreakable:1,ench:[{id:19,lvl:1000}]}" };
     
 
     for (String NBT : array) {
       ItemStack diamond_sword = new ItemStack(Items.DIAMOND_SWORD);
       ItemStack golden_sword = new ItemStack(Items.GOLDEN_SWORD);
       ItemStack iron_sword = new ItemStack(Items.IRON_SWORD);
       ItemStack stone_sword = new ItemStack(Items.STONE_SWORD);
       ItemStack wooden_sword = new ItemStack(Items.WOODEN_SWORD);
       ItemStack stick = new ItemStack(Items.STICK);
       ItemStack[] array1 = { diamond_sword, golden_sword, iron_sword, stone_sword, wooden_sword, stick };
       
       for (ItemStack stack : array1) {
         try {
           stack.setTagCompound(JsonToNBT.getTagFromJson(NBT));
         }
         catch (NBTException localNBTException) {}
         p_78018_1_.add(stack);
       }
       
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
       p_78018_1_.add(new ItemStack(Items.AIR));
     }
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
