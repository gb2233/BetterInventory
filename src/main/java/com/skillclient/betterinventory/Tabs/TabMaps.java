 package com.skillclient.betterinventory.Tabs;

 import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class TabMaps
   extends CreativeTabs
 {
   public TabMaps(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   @Override public ItemStack getTabIconItem()
   {
	 this.setBackgroundImageName("item_search.png");
     return new ItemStack(Items.FILLED_MAP);
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
	 Items.COMMAND_BLOCK_MINECART.setCreativeTab(this);
	 p_78018_1_.add(new ItemStack(Items.FILLED_MAP, 1, 3));
     for (int m = 0; m <= 2304; m++) {
     p_78018_1_.add(new ItemStack(Items.FILLED_MAP, 1, m));
   }
   }

   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
