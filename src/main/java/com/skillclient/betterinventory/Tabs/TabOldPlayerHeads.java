 package com.skillclient.betterinventory.Tabs;
 
import java.time.LocalDate;

import com.skillclient.betterinventory.util.Download;
import com.skillclient.betterinventory.util.ItemStacks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class TabOldPlayerHeads extends CreativeTabs
 {
   ItemStack empty;
   public TabOldPlayerHeads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
	 LocalDate localDate = LocalDate.now();
	 if(localDate == LocalDate.of(localDate.getYear(), 6, 28))
		 return ItemStacks.stringtostack("minecraft:skull 1 3 {SkullOwner:{Id:\"e14748a3-fd60-4feb-9fce-7a6c7389a948\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTExOWZjYTRmMjhhNzU1ZDM3ZmJlNWRjZjZkOGMzZWY1MGZlMzk0YzFhNzg1MGJjN2UyYjcxZWU3ODMwM2M0YyJ9fX0=\"}]}}}");		 
	 else
		 return ItemStacks.stringtostack("minecraft:skull 1 3 {SkullOwner:{Id:\"62f5b528-e9d7-4e26-b069-fc9e339e4079\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWNmNDNiYzFhZWNmYmU5MDNlN2FlNDQ0ZjY3NzdiNzkwNDZiOGRlNjQ1ODRjYjZhYTk4MmEzNWEyIn19fQ==\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.OldPlayerHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
