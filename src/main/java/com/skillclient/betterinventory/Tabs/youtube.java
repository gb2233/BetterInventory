 package com.skillclient.betterinventory.Tabs;
 
 import com.skillclient.betterinventory.util.Download;

 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class youtube
   extends CreativeTabs
 {
	 ItemStack empty;
   public youtube(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
     setBackgroundImageName("item_search.png");
   }
 
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.DIAMOND);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.YoutubeStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
