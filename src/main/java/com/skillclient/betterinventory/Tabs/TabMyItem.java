 package com.skillclient.betterinventory.Tabs;
 
 import java.util.ArrayList;
 import java.util.List;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Blocks;
 import net.minecraft.item.Item;
 import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
 
 public class TabMyItem extends CreativeTabs
 {
   public static ArrayList MyItemStacks = new ArrayList();
   
   public TabMyItem(int id, String unlocalizedName) {
	 super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }
 
   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Item.getItemFromBlock(Blocks.COMMAND_BLOCK));
   }

   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(MyItemStacks);
   }
   
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
