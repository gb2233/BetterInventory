 package com.skillclient.betterinventory.Tabs;

 import com.skillclient.betterinventory.util.Download;
 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;

 import java.time.LocalDate;

 public class TabPlantHeads extends CreativeTabs
 {
   ItemStack empty;
   public TabPlantHeads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
       return ItemStacks.stringtostack("minecraft:skull 1 3 {display:{Name:\"Melon\"},SkullOwner:{Id:\"ffb2d526-388f-4164-89b9-f4ce2bdf611f\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmFhZTI5Nzg1OTgyYmY4MGMxM2M0ZDY4ZmFmYmMyNjdhNjY3ZDAxM2VkOWZlY2I0YWE5YTNhNzQxZmFlNDM4MyJ9fX0=\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.PlantHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
