 package com.skillclient.betterinventory.Tabs;

 import com.skillclient.betterinventory.util.Download;
 import com.skillclient.betterinventory.util.ItemStacks;
 import net.minecraft.creativetab.CreativeTabs;
 import net.minecraft.init.Items;
 import net.minecraft.item.ItemStack;
 import net.minecraft.util.NonNullList;

 import java.time.LocalDate;

 public class TabAlphabetHeads extends CreativeTabs
 {
   ItemStack empty;
   public TabAlphabetHeads(int id, String unlocalizedName)
   {
     super(id, unlocalizedName);
	 setBackgroundImageName("item_search.png");
   }

   @Override public ItemStack getTabIconItem()
   {
     return new ItemStack(Items.SKULL);
   }
   
   @Override
   public boolean hasSearchBar() {
       return true;
   }
   
   public ItemStack getIconItemStack()
   {
       return ItemStacks.stringtostack("minecraft:skull 1 3 {display:{Name:\"Lettercube A\"},SkullOwner:{Id:\"45956d10-8438-4670-bf2b-0dc5bb3a05ef\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGZmODhiMTIyZmY5MjUxM2M2YTI3YjdmNjdjYjNmZWE5NzQzOWUwNzg4MjFkNjg2MWI3NDMzMmEyMzk2In19fQ==\"}]}}}");
   }

   @Override public void displayAllRelevantItems(NonNullList<ItemStack> p_78018_1_)
   {
     p_78018_1_.addAll(Download.AlphabetHeadsStacks);
   }
   public String getTranslatedTabLabel(){
	   return this.getTabLabel();
   }
 }
